from os import environ
from os.path import join as pjoin

from math import sqrt
from cmath import exp
from cmath import polar

from operator import add


from DatabaseInterface import DBInterface, ReferenceTable


gval = .85



class BlochTable(ReferenceTable):
    def __init__(self,DB:DBInterface,Name:str):
        super().__init__(DB,'Bloch_'+Name,'gx,gy,gz,Dabs,Darg')

class BlochCoeffTable(ReferenceTable):
    def __init__(self,DB:DBInterface,Name:str):
        super().__init__(DB,'Bloch_Coeffs_'+Name,'g,Dabs,Darg')

class BlochKeyTable(ReferenceTable):
    def __init__(self,DB:DBInterface,Name:str):
        super().__init__(DB,'Bloch_Keys_'+Name,'g,gx,gy,gz,p,n')


Loc = environ.get("blochDataDir")



# Import Bloch function from text file
RawGambleDict = {}
with open(pjoin(Loc,'BlochCoeffsPZ.txt'),'rt') as fin:
    for lin in fin:
        lin = lin.split('#',1)[0]
        lin = lin.split()
        if lin:
            try:
                key = tuple(int(x) for x in lin[:3])
                value = complex(*tuple(float(x) for x in lin[3:]))
            except ValueError:
                continue
            RawGambleDict[key] = value
del fin,lin,key,value


# Symmetrize Bloch functions
GambleDict = {}
for key in RawGambleDict:

    if key not in GambleDict:

        KeySetIden = {
            (key[0],key[1],key[2]),
            (key[1],key[0],key[2]),
            (-key[0],-key[1],key[2]),
            (-key[1],-key[0],key[2]),
            }

        KeySetConj = {
            (key[0],-key[1],key[2]),
            (key[1],-key[0],key[2]),
            (-key[0],key[1],key[2]),
            (-key[1],key[0],key[2]),
            }

        a1 = sum(RawGambleDict[x] for x in KeySetIden)
        a2 = sum(RawGambleDict[x].conjugate() for x in KeySetConj)
        value = (a1+a2)/(len(KeySetIden)+len(KeySetConj))

    for key1 in KeySetIden:
        if key1 not in GambleDict:
            GambleDict[key1] = value
    for key1 in KeySetConj:
        if key1 not in GambleDict:
            GambleDict[key1] = value.conjugate()
del RawGambleDict
del KeySetConj, KeySetIden
del key, key1
del a1, a2, value


# Normalize the symmetrized Bloch function
norm = 1/sqrt( abs(sum(x.conjugate()*x for x in GambleDict.values())) )
# GambleDict = { k:norm*v for k,v in GambleDict.items() }
GambleDict.update( (k,norm*v) for k,v in GambleDict.items() )
del norm


# Open database, save full symmetrized expression for Bloch function
Z_Gamble_DB = DBInterface('Bloch_Z_Gamble_Full.db',Loc)
T = GambleDict
K = sorted( T, key=lambda X:sum([x**2 for x in X]) )
R = tuple( k+polar(T[k]) for k in  K )
BlochTable(Z_Gamble_DB,'Z_Gamble').Insert(R)
del T, K, R
del Z_Gamble_DB




def CalcRawProduct(BlochDict,tuplefunc):
    RawGBloch = {}
    for k1,v1 in BlochDict.items():
        for k2,v2 in BlochDict.items():
            key = tuplefunc(k1,k2)
            value = v1*v2
            if key not in RawGBloch:
                RawGBloch[key] = value
            else:
                RawGBloch[key] += value

    return RawGBloch
# Remember that k dot r is a DOT product
# Forward transformation of r duals with inverse transformation of k
def ZZ_tuplefunc(k1,k2): return tuple(map(add,k2,k1))
def bZZ_tuplefunc(k1,k2):
    return tuple(map(add,(-k1[0],k1[1],-k1[2],),(k2[0],k2[1],k2[2],)))
def YX_tuplefunc(k1,k2):
    return tuple(map(add,(k1[1],k1[2],k1[0],),(k2[2],k2[0],k2[1],)))
def bYX_tuplefunc(k1,k2):
    return tuple(map(add,(-k1[1],-k1[2],k1[0],),(k2[2],k2[0],k2[1],)))
bZZ_RawGBloch,ZZ_RawGBloch,bYX_RawGBloch,YX_RawGBloch = tuple(
    CalcRawProduct(GambleDict,x)
    for x in (bZZ_tuplefunc,ZZ_tuplefunc,bYX_tuplefunc,YX_tuplefunc)
)


def SymmetricBlochProduct(RawGBloch,SymmFunc):
    GBloch = {}
    for key in RawGBloch:

        if key not in GBloch:

            KeySetIden,KeySetConj = SymmFunc(key)

            a1 = sum(RawGBloch[x] for x in KeySetIden)
            a2 = sum(RawGBloch[x].conjugate() for x in KeySetConj)
            value = (a1+a2)/(len(KeySetIden)+len(KeySetConj))

        for key1 in KeySetIden:
            if key1 not in GBloch:
                GBloch[key1] = polar(value)
        for key1 in KeySetConj:
            if key1 not in GBloch:
                GBloch[key1] = polar(value.conjugate())

    return GBloch
def bZZ_symmfunc(key):
    KeySetIden = {
        (key[0],key[1],key[2]),
        (key[1],key[0],key[2]),
        (-key[0],-key[1],key[2]),
        (-key[1],-key[0],key[2]),
        (key[0],-key[1],-key[2]),
        (key[1],-key[0],-key[2]),
        (-key[0],key[1],-key[2]),
        (-key[1],key[0],-key[2]),
        }
    KeySetConj = {
        (-key[0],-key[1],-key[2]),
        (-key[1],-key[0],-key[2]),
        (key[0],key[1],-key[2]),
        (key[1],key[0],-key[2]),
        (-key[0],key[1],key[2]),
        (-key[1],key[0],key[2]),
        (key[0],-key[1],key[2]),
        (key[1],-key[0],key[2]),
        }
    return (KeySetIden,KeySetConj,)
def ZZ_symmfunc(key):
    KeySetIden = {
        (key[0],key[1],key[2]),
        (key[1],key[0],key[2]),
        (-key[0],-key[1],key[2]),
        (-key[1],-key[0],key[2]),
    }
    KeySetConj = {
        (key[0],-key[1],key[2]),
        (key[1],-key[0],key[2]),
        (-key[0],key[1],key[2]),
        (-key[1],key[0],key[2]),
    }
    return (KeySetIden,KeySetConj,)
def bYX_symmfunc(key):
    KeySetIden = {
        (key[0],key[1],key[2]),
        (-key[1],-key[0],key[2]),
    }
    KeySetConj = {
        (key[0],key[1],-key[2]),
        (-key[1],-key[0],-key[2]),
    }
    return (KeySetIden,KeySetConj,)
def YX_symmfunc(key):
    KeySetIden = {
        (key[0],key[1],key[2]),
        (key[1],key[0],key[2]),
    }
    KeySetConj = {
        (key[0],key[1],-key[2]),
        (key[1],key[0],-key[2]),
    }
    return (KeySetIden,KeySetConj,)
def SymmetrizeBloch(X):
    return tuple(
        {
            tuple(float(sum(x)) for x in zip(k,x[0])):v
            for k,v in SymmetricBlochProduct(x[1],x[2]).items()
        }
        for x in X
    )
bZZ_GBloch,ZZ_GBloch,bYX_GBloch,YX_GBloch = SymmetrizeBloch((
    ((0.,0.,0.),bZZ_RawGBloch,bZZ_symmfunc),
    ((0.,0.,2*gval),ZZ_RawGBloch,ZZ_symmfunc),
    ((gval,-gval,0.),bYX_RawGBloch,bYX_symmfunc),
    ((gval,gval,0.),YX_RawGBloch,YX_symmfunc),
))
del bZZ_RawGBloch,ZZ_RawGBloch,YX_RawGBloch,bYX_RawGBloch



# Open database, save full symmetrized expression for Bloch products
bZZ_Gamble_DB = DBInterface('Bloch_bZZ_Gamble_Full.db',Loc)
ZZ_Gamble_DB = DBInterface('Bloch_ZZ_Gamble_Full.db',Loc)
bYX_Gamble_DB = DBInterface('Bloch_bYX_Gamble_Full.db',Loc)
YX_Gamble_DB = DBInterface('Bloch_YX_Gamble_Full.db',Loc)
for x in (
    (bZZ_GBloch,bZZ_Gamble_DB,'bZZ_Gamble'),
    (ZZ_GBloch,ZZ_Gamble_DB,'ZZ_Gamble'),
    (bYX_GBloch,bYX_Gamble_DB,'bYX_Gamble'),
    (YX_GBloch,YX_Gamble_DB,'YX_Gamble'),
):
    # T = x[0]
    # K = sorted( T, key=lambda X:sum([x**2 for x in X]) )
    # R = tuple( k+T[k] for k in  K )
    T = x[0]
    R = tuple( k+T[k] for k in  T )
    BlochTable(x[1],x[2]).Insert(R)
del x, T, R


del bZZ_Gamble_DB, ZZ_Gamble_DB, bYX_Gamble_DB, YX_Gamble_DB


# These terms of the Bloch products are dealt with seperately
# labeled as the "naive Bloch" result
del bZZ_GBloch[(0.,0.,0.)]
del ZZ_GBloch[(0.,0.,2*gval)]
del bYX_GBloch[(gval,-gval,0.)]
del YX_GBloch[(gval,gval,0.)]


def bZZ_symmfunc1(key):
    Trans = (
        (
            (+1,0),
            {
                (key[0],key[1],key[2]),
                (key[1],key[0],key[2]),
                (-key[0],-key[1],key[2]),
                (-key[1],-key[0],key[2]),
                (key[0],-key[1],-key[2]),
                (key[1],-key[0],-key[2]),
                (-key[0],key[1],-key[2]),
                (-key[1],key[0],-key[2]),
            }
        ),
    )
    KeySet = set()
    KeySetNull = set()
    for (v,S) in Trans:
        for k in S:
            if k not in KeySetNull:
                KeySet.add(k+v)
                KeySetNull.add((-k[0],-k[1],-k[2],))
    return KeySet,KeySetNull
def ZZ_symmfunc1(key):
    Trans = (
        (
            (+1,0),
            {
                (key[0],key[1],key[2]),
                (key[1],key[0],key[2]),
                (-key[0],-key[1],key[2]),
                (-key[1],-key[0],key[2]),
            }
        ),
        (
            (-1,0),
            {
                (key[0],-key[1],-key[2]),
                (key[1],-key[0],-key[2]),
                (-key[0],key[1],-key[2]),
                (-key[1],key[0],-key[2]),
            }
        ),
    )
    KeySet = set()
    KeySetNull = set()
    for (v,S) in Trans:
        for k in S:
            if k not in KeySetNull:
                KeySet.add(k+v)
                KeySetNull.add((-k[0],-k[1],-k[2],))
    return KeySet,KeySetNull
def YX_symmfunc1(key):
    Trans = (
        (
            (+1,0),
            {
                (key[0],-key[1],-key[2]),
                (key[1],-key[0],-key[2]),
            }
        ),
        (
            (-1,0),
            {
                (key[0],-key[1],key[2]),
                (key[1],-key[0],key[2]),
            }
        ),
        (
            (+1,1),
            {
                (key[0],key[1],key[2]),
                (key[1],key[0],key[2]),
            }
        ),
        (
            (-1,1),
            {
                (key[0],key[1],-key[2]),
                (key[1],key[0],-key[2]),
            }
        ),
    )
    KeySet = set()
    KeySetNull = set()
    for (v,S) in Trans:
        for k in S:
            if k not in KeySetNull:
                KeySet.add(k+v)
                KeySetNull.add((-k[0],-k[1],-k[2],))
    return KeySet,KeySetNull
# Calculate the refined Bloch product
# extract lists of coefficients and Fourier vectors
# insert each list into database
def GroupBlochProductCoeffs(GBloch,ProdSpec,symmfunc1):
    g = 0
    AddedTo = set()
    CoeffList = []
    KeyList = []

    for key in sorted(GBloch, key=lambda X:sum([x**2 for x in X])):
        if key in AddedTo:
            continue
        CoeffList.append((g,)+GBloch[key])
        KeySet,KeySetNull = symmfunc1(key)
        for keyh in KeySet:
            if keyh[:3] not in AddedTo:
                AddedTo.add(keyh[:3])
                KeyList.append((g,)+keyh)
        for keyh in KeySetNull:
            AddedTo.add(keyh)
        g += 1
    # Coeffs_DB = DBInterface('Bloch_'+ProdSpec+'_Coeffs.db',Loc)
    # Keys_DB = DBInterface('Bloch_'+ProdSpec+'_Keys.db',Loc)
    Bloch_DB = DBInterface('Bloch.db',Loc)
    BlochCoeffTable(Bloch_DB,ProdSpec).Insert( tuple(CoeffList) )
    BlochKeyTable(Bloch_DB,ProdSpec).Insert( tuple(KeyList) )



# Calculate the refined Bloch product
GroupBlochProductCoeffs(bZZ_GBloch,'bZZ_Gamble',bZZ_symmfunc1)
GroupBlochProductCoeffs(ZZ_GBloch,'ZZ_Gamble',ZZ_symmfunc1)
GroupBlochProductCoeffs(YX_GBloch,'YX_Gamble',YX_symmfunc1)
