from enum import Enum
import sqlite3
from os import getcwd
from os.path import join as pjoin
from itertools import chain, islice, starmap
from typing import Iterable

DatabaseProvider = sqlite3
DatabaseProviderIdent = 'sqlite3'
DBDelimiter = '?'

# DatabaseProvider = psycopg2
# DatabaseProviderIdent = 'psycopg2'
# DatabaseDelimiter = '%s'



# Chunkify returns an iterator that breaks iterables with a large number of
# elements into smaller chunks.
def Chunkify(gen,ChunkSize):
    if gen is not None:
        itr = iter(gen)
        for first in itr:
            yield chain([first],islice(itr,ChunkSize-1))
    else:
        return # Nothing to Chunkify

def Tupleize(x):
    try:
        T = tuple(x)
    except TypeError:
        T = (x,)
    return T

def PrependData(X,c):
        return ( chain(c, x) for x in X )


def MergeZip(*Z):
    return ( chain(*z) for z in zip(*Z) )

class DBInterface():

    def __init__(self,Name:str,Loc:str=getcwd()):

        # Where is the database file located?
        self.__Loc = Loc
        # What is the filename of the database?
        self.__Name = Name

        x = pjoin(Loc,Name)
        self.__conn = DatabaseProvider.connect(x)

    def __del__(self):
        self.__conn.close()

    def Execute(self,s:str,params=None):
        if params is None:
            return self.__conn.execute(s)
        else:
            return self.__conn.execute(s,params)

    def ExecuteMany(self,s:str,param_seq=None):
        return self.__conn.executemany(s,param_seq)

    def Vacuum(self):
        self.__conn.execute("VACUUM")

    def Commit(self):
        self.__conn.commit()





class TableInterface():

    def __init__(self,
        DB:DBInterface,
        TableName:str,
        ColLabels:str,
        **kwargs
    ):

        # Which groups of columns (if any) are unique?
        try:
            self.UniqueCols:str = kwargs["UniqueCols"]
        except KeyError:
            self.UniqueCols:str = None

        

        # Store database object.
        self.DB = DB
        # What is the name of the table?
        self.TableName = TableName
        # What are the column labels of the table?
        self.ColLabels = ColLabels
        
        self._MakeTable()
        self._MakeIndex()
        self.RowsNotCounted = True

    def _MakeTable(self):

        subs = {
            "Named":self.TableName,
            "WithCols":self.ColLabels
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='table'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                "CREATE TABLE IF NOT EXISTS {Named} ({WithCols})"
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

        self.NLabels = len(self.ColLabels.split(','))
        self.Mask = "({})".format(','.join((DBDelimiter,)*self.NLabels))

    def _MakeIndex(self):
        if self.UniqueCols is not None:
            subs = {
                "Named":self.TableName+"_"+"Uniqs",
                "Table":self.TableName,
                "WithCols":self.UniqueCols
            }

            comm = (
                "SELECT * FROM sqlite_schema WHERE type='index'"
                +" and name='{Named}'"
            ).format(**subs)
            if self.DB.Execute(comm).fetchone() is None:
                comm = (
                    "CREATE UNIQUE INDEX IF NOT EXISTS"
                    +" {Named} ON {Table} ({WithCols})"
                ).format(**subs)
                self.DB.Execute(comm)
                self.DB.Commit()

    def _GetRows(self,Cols:str='*',start:int=0,end:int=None):
        if end is None:
            RowSpec = "WHERE rowid >= "+str(start)
        else:
            RowSpec = " WHERE rowid BETWEEN {} AND {}".format(start,end)
        subs = {
            "Cols":Cols,
            "Table":self.TableName,
            "RowSpec":RowSpec
        }

        comm = 'SELECT {Cols} FROM {Table} {RowSpec}'.format(**subs)
        return self.DB.Execute(comm)

    def _GetNumRows(self):
        res = self.DB.Execute('SELECT COUNT(*) FROM {}'.format(self.TableName))
        return next(res)[0]

    def _MatchRows(self,RetCols:str,InCols:str,Iter):
        # Cols holds a string specifying which columns for the Match to return.
        # Keys holds a string over which columns to check for Match.
        # Iter holds an iterable over tuples of columns values to Match.

        # Break a large iterator into smaller chunks.
        Iter = Chunkify(Iter,32766)

        res = iter(())

        for Chunk in Iter:

            Query = tuple(Chunk)
            NQueries = len(Query)
            if NQueries == 0:
                return res
            NCols = len(InCols.split(','))

            tmask = "({})".format(','.join((DBDelimiter,)*NCols))
            qmask = ','.join((tmask,)*NQueries)

            subs = {
                'RetCols':RetCols,
                'Table':self.TableName,
                'InCols':InCols,
                'qMask':qmask
            }
            comm = (
                "SELECT {RetCols} FROM {Table}"
                +" WHERE ({InCols}) IN (VALUES {qMask})"
            ).format(**subs)
            Query = tuple(x for q in Query for x in q)

            res = chain(res, self.DB.Execute(comm,Query))

        return res

    def _Insert(self,Rows):
        subs = {
            'Table':self.TableName,
            'Cols':self.ColLabels,
            'Mask':self.Mask
        }
        comm = (
            'INSERT OR IGNORE INTO {Table} ({Cols}) VALUES {Mask}'
        ).format(**subs)

        # Break a large iterator into smaller chunks.
        Iter = Chunkify(Rows,32766)
        for i,Chunk in enumerate(Iter):
            rows = tuple( tuple(row) for row in Chunk )
            self.DB.ExecuteMany(comm,rows)
        try:
            if i>=0:
                self.DB.Commit()
                self.RowsNotCounted = True
        except:
            return # Insert didn't happen

    def TableSize(self):
        if (self.RowsNotCounted):
            self.__TableSize = self._GetNumRows()
            self.RowsNotCounted = False
        return self.__TableSize

    def CheckIds(self,Ids):
        IterOfIds = ((i,) for i in Ids ) 
        return (row[0] for row in self._MatchRows('rowid','rowid',IterOfIds))

    def CheckRows(self,Rows):
        if self.UniqueCols is not None:
            return self._MatchRows(self.UniqueCols,self.UniqueCols,Rows)
        else:
            return iter(())

    def FindNewRows(self,Rows):
        Rows = tuple(Rows)
        RowsOld = set(self.CheckRows(Rows))
        return (x for x in Rows if x not in RowsOld)

    def Insert(self,Rows):
        self._Insert(self.FindNewRows(Rows))





class ReferenceTable(TableInterface):
    # Each row must be unique in this table.
    # sqlite3 associates each table row with a unique identifier (the rowid).
    def __init__(self,DB:DBInterface,Name:str,KeyLabels:str):
        super().__init__(DB,Name,KeyLabels,UniqueCols=KeyLabels)

    def GetIds(self,Keys):
        return (x[0] for x in self._MatchRows('rowid',self.UniqueCols,Keys))

    def FindKeys(self,Ids):
        return (x for x in self._MatchRows(self.UniqueCols,'rowid',Ids))

    def GetReferenceRows(self,Keys):

        RetCols = 'rowid'+','+self.UniqueCols
        return (x for x in self._MatchRows(RetCols,self.UniqueCols,Keys))

    def Insert(self,Rows):
        self._Insert(self.FindNewRows(Rows))
        return self._GetRows()






class KeyValTable(TableInterface):
    # Table to access databse.
    # This class makes a distinction between "Key" columns and "Val" columns.
    # This is to construct a Key->Val mapping with unique keys.
    # Values are calculated at Key insertion.
    def __init__(
        self,
        DB:DBInterface,TableName:str,
        KeyLabels:str,ValLabels:str,Conf:Enum=None
    ):

        self.ValLabels = ValLabels
        self.Conf = Conf

        TableName = TableName+('' if Conf is None else '_'+Conf.name )
        Cols = KeyLabels+','+ValLabels
        Uniqs = KeyLabels

        TableInterface.__init__(
            self,DB,TableName,Cols,
            UniqueCols=Uniqs,
        )

    def CheckKeys(self,Keys):
        return self._MatchRows(self.UniqueCols,self.UniqueCols,Keys)

    def FindNewRows(self,Keys):
        NewKeys = tuple(super().FindNewRows(Keys))
        if self.Conf is None:
            def F(x):
                return self.MapVal(x)
        else:
            def F(x):
                return self.MapVal(x,self.Conf.value)
        # return ( tuple(x) for p in starmap(F,NewKeys) for x in p)
        R = tuple(starmap(F,NewKeys))
        return ( tuple(x) for x in MergeZip(NewKeys,R) )

    @staticmethod
    def UnpackValues(V):
        return V[0]

    @staticmethod
    def PackValues(v):
        return Tupleize(v)

    @classmethod
    def MapVal(cls,x):
        return cls.PackValues(x)


class RefValTable(TableInterface):
    def __init__(
            self,
            DB:DBInterface,TableName:str,
            KeyLabels:str,IdxLabels:str,ValLabels:str,
            RefName:str,
            ConfInst:Enum=None
        ):
        
        self.KeyLabels = KeyLabels
        self.IdxLabels = IdxLabels
        self.ValLabels = ValLabels
        self.ConfInst = ConfInst

        if ConfInst is not None:
            TableName = TableName+'_'+ConfInst.name
                
        Cols = 'KeyId,'+KeyLabels+','+IdxLabels+','+ValLabels
        Uniqs = KeyLabels+','+IdxLabels
        TableInterface.__init__(
            self,DB,TableName,Cols,
            UniqueCols=Uniqs,
        )

        self.RefName = RefName
        self.RefTable = ReferenceTable(DB,RefName,KeyLabels)

    def _MakeIndex(self):

        super()._MakeIndex()

        subs = {
            "Named":self.TableName+"_"+"Key",
            "Table":self.TableName,
            "WithCols":self.KeyLabels
        }
        comm = (
            "SELECT * FROM sqlite_schema WHERE type='index'"
            +" and name='{Named}'"
        ).format(**subs)
        if self.DB.Execute(comm).fetchone() is None:
            comm = (
                "CREATE INDEX IF NOT EXISTS"
                +" {Named} ON {Table} ({WithCols})"
            ).format(**subs)
            self.DB.Execute(comm)
            self.DB.Commit()

    def Insert(self,Keys:Iterable):
        Keys = tuple(Keys)
        Keys = self.RefTableInsert(Keys)
        super().Insert(Keys)

    def RefTableInsert(self,KeyParts:Iterable):
        Keys = self.GenerateKeys(KeyParts)
        return self.RefTable.Insert(Keys)

    def GenerateKeys(self,KeyParts:Iterable):
        return tuple(KeyParts)


    def FindNewRows(self,Keys):
        NewKeys = tuple(super().FindNewRows(Keys))
        D = { x[1:]:x[0] for x in self.RefTable.GetReferenceRows(NewKeys) }
        def F(*x):
            return self.MapVal(x[0],x[1])
        X = tuple(self.KeyIdxIter(NewKeys))
        I = tuple( (D[k],) for (k,_) in X )
        R = tuple(starmap(F,X))
        Y = tuple(( key+idx for key,idx in X ))
        return ( tuple(x) for x in MergeZip(I,Y,R) )

    def CheckKeys(self,Keys):
        return self.RefTable.CheckRows(Keys)

    def CheckRows(self,Rows):
        return self._MatchRows(self.KeyLabels,self.KeyLabels,Rows)

    def GetKeyIds(self,Keys):
        return self.RefTable.GetIds(Keys)

    def KeyIdxIter(self,Keys):
        for key in Keys:
            for Idx in self.IdxTuple(key):
                yield (key,Idx)

    @staticmethod
    def IdxTuple(Key,ConfInst=None):
        return tuple( tuple() for Idx in () )

    @staticmethod
    def UnpackValues(V):
        return V[0]

    @staticmethod
    def PackValues(v):
        return Tupleize(v)

    @staticmethod
    def MapVal(Key,Idx):
        pass